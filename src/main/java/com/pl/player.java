package com.pl;

import org.hibernate.annotations.Table;

import javax.persistence.Entity;

import javax.persistence.Id;

    @Entity

    @Table(appliesTo = "")

    public class player {
        @Id
        private int playerId;
        private String playerName;
        private String teamName;
        private int age;


        public player() {



        }


        /**
         * @return the playerId
         */
        public int getPlayerId() {
            return playerId;
        }


        /**
         * @param playerId the playerId to set
         */
        public void setPlayerId(int playerId) {
            this.playerId = playerId;
        }


        /**
         * @return the playerName
         */
        public String getPlayerName() {
            return playerName;
        }


        /**
         * @param playerName the playerName to set
         */
        public void setPlayerName(String playerName) {
            this.playerName = playerName;
        }


        /**
         * @return the teamName
         */
        public String getTeamName() {
            return teamName;
        }


        /**
         * @param teamName the teamName to set
         */
        public void setTeamName(String teamName) {
            this.teamName = teamName;
        }


        /**
         * @return the age
         */
        public int getAge() {
            return age;
        }


        /**
         * @param age the age to set
         */
        public void setAge(int age) {
            this.age = age;
        }


        @Override
        protected Object clone() throws CloneNotSupportedException {

            return super.clone();
        }



        public player(int playerId, String playerName, String teamName, int age) {
            super();
            this.playerId = playerId;
            this.playerName = playerName;
            this.teamName = teamName;
            this.age = age;
        }


        @Override
        public String toString() {
            return "player [playerId=" + playerId + ", playerName=" + playerName + ", teamName=" + teamName + ", age=" + age
                    + ", getClass()=" + getClass() + ", toString()=" + super.toString() + "]";
        }


    }


