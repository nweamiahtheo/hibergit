package com.pl;



import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;


import org.hibernate.query.Query;

public class HibernateMain {

    public static void main(String[] args) {
        try {
            Configuration configuration = new Configuration().configure();

            configuration.addAnnotatedClass(com.pl.player.class);
            StandardServiceRegistryBuilder builder =
                    new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
            SessionFactory factory = configuration.buildSessionFactory(builder.build());

            Session session = factory.openSession();
            Transaction transaction = session.beginTransaction();
            Query query = (Query) new session.creatQuery("from player");
            player p1 = new player(10, "Messi", "Agetina", 30);
            player p2 = new player(8, "Mes", "Mata", 30);
            session.save(p1);
            session.save(p2);
            transaction.commit();
            session.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}